import {useState, useRef, useEffect} from 'react';

import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

//code ni sir
// yung data prop is not predefined, tayo naglagay there 
const GlobalMap = ({data}) => {

	const mapContainerRef = useRef(null)
	
	const [latitude, setLatitude] = useState(0);
	const [longitude, setLongitude] = useState(0);
	const [zoom, setZoom] = useState(0);

	useEffect(() => {
		//data.country_name siya kasi nareretrieve natin yung buong data, so if we want the country name, we just need to specify na country_name :)	
		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${data.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
				.then(res => res.json())
				.then(data => {
					console.log(data)

					/*from data: 
					features:
					0:
					center: [19.4993490091469, 47.0602667471763]*/
					setLongitude(data.features[0].center[0])
					setLatitude(data.features[0].center[1])
					setZoom(1)
				})

		const map = new mapboxgl.Map({
		    //set the container for the map as the current component
		    container: mapContainerRef.current,
		    //set the style for the map
		    style: 'mapbox://styles/mapbox/streets-v11',
		    center: [longitude,latitude],
		    zoom: zoom
		    })

		    map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
		    
		    const marker = new mapboxgl.Marker()
		    .setLngLat([longitude,latitude])
		    .addTo(map)
	}, [])

	return <div className="mapContainer" ref={mapContainerRef} />
}

export default GlobalMap

// my code
/*export default function GlobalMap({country}) {

	const [latitude, setLatitude] = useState(0);
  	const [longitude, setLongitude] = useState(0);
  	const [zoom, setZoom] = useState(0);

  	const mapContainerRef = useRef(null)

	useEffect(() => {
	    const map = new mapboxgl.Map({
	    //set the container for the map as the current component
	    container: mapContainerRef.current,
	    //set the style for the map
	    style: 'mapbox://styles/mapbox/streets-v11',
	    center: [longitude,latitude],
	    zoom: zoom
	    })

	    map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
	    const marker = new mapboxgl.Marker()
	    .setLngLat([longitude,latitude])
	    .addTo(map)

		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
	      .then(res => res.json())
	      .then(data => {

	      setLongitude(data.features[0].center[0])
	      setLatitude(data.features[0].center[1])
	      setZoom(1)
	    })
	}, [])

	return (
		<div className="mapContainer" ref={mapContainerRef} />
	)
}*/