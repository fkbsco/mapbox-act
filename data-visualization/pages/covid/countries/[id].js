import {Row, Col} from 'react-bootstrap';

import toNum from '../../../helpers/toNum';
import DoughnutChart from '../../../components/DoughnutChart';
import Banner from '../../../components/Banner';
import GlobalMap from '../../../components/GlobalMap';

export default function country({country}){

  // const [latitude, setLatitude] = useState(0);
  // const [longitude, setLongitude] = useState(0);
  // const [zoom, setZoom] = useState(0);

  // const mapContainerRef = useRef(null)

  // useEffect(() => {
  //   const map = new mapboxgl.Map({
  //   //set the container for the map as the current component
  //   container: mapContainerRef.current,
  //   //set the style for the map
  //   style: 'mapbox://styles/mapbox/streets-v11',
  //   center: [longitude,latitude],
  //   zoom: zoom
  //   })

  //   map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
  //   const marker = new mapboxgl.Marker()
  //   .setLngLat([longitude,latitude])
  //   .addTo(map)

  //   fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
  //     .then(res => res.json())
  //     .then(data => {

  //     setLongitude(data.features[0].center[0])
  //     setLatitude(data.features[0].center[1])
  //     setZoom(1)
  //   })
  // })

  //code ko
  /*return (
      <React.Fragment>
        <Banner
          country = {country.country_name}
          deaths = {country.deaths}
          criticals = {country.serious_critical}
          recoveries = {country.total_recovered}
        />
        <Row>
          <Col xs={12} md={6}>
            <DoughnutChart
              criticals = {toNum(country.serious_critical)}
              deaths = {toNum(country.deaths)}
              recoveries = {toNum(country.total_recovered)}
            />
          </Col>
          <Col xs={12} md={6}>
            <GlobalMap country = {country.country_name} />
          </Col>
        </Row>
      </React.Fragment>
    )
  }*/

  //code ni sir 
	return (
		<React.Fragment>
			<Banner
				country = {country.country_name}
				deaths = {country.deaths}
				criticals = {country.serious_critical}
				recoveries = {country.total_recovered}
			/>
      <Row>
        <Col xs={12} md={6}>
    			<DoughnutChart
    				criticals = {toNum(country.serious_critical)}
    				deaths = {toNum(country.deaths)}
    				recoveries = {toNum(country.total_recovered)}
    			/>
        </Col>
        <Col xs={12} md={6}>
          <GlobalMap data={country}/>
        </Col>
      </Row>
		</React.Fragment>
	)
}

// generates the dynamic routes
export async function getStaticPaths() {
// fetch data from api endpoint
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
  })

  const data = await res.json()

  const paths = data.countries_stat.map(country => {
  	if (country.country_name.includes(' ')) {
      return { params: { id: country.country_name.replace(' ', '%20') } };
    } else {
      return { params: { id: country.country_name } };
    }
  })
  // you can also remove the space instead haha

  // return props
  return {paths, fallback: false}
}

export async function getStaticProps({params}) {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
  })

  const data = await res.json()

  const country = data.countries_stat.find(country => country.country_name === params.id)

  return {
    props: {
      country
    }
  }
}