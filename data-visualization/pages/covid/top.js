import { useState, useEffect, useRef } from 'react';
import { Doughnut } from 'react-chartjs-2';

import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

import toNum from '../../helpers/toNum';

export default function Top({ data }) {

	//code ni sir
	console.log(data)

	const countriesStats = data.countries_stat;
	console.log(countriesStats);

	const mapContainerRef = useRef(null)

	const countriesCases = countriesStats.map(countryStat => {
		return {
			name: countryStat.country_name,
			cases: toNum(countryStat.cases)
		}
	})

	console.log(countriesCases)

	const countriesArray = countriesCases.sort((a,b) => {
		if(a.cases < b.cases){
			return 1 // a will move to the right
		} else if (a.cases > b.cases) {
			return -1 // a will move to the left
		} else {
			return 0
		}
	}).slice(0,10)

	console.log(countriesArray)

	let countryNames = []

	countriesArray.forEach(country => {
		return countryNames.push(country.name)
	})

	console.log(countryNames)

	let countryCases = []

	countriesArray.forEach(country => {
		return countryCases.push(country.cases)
	})

	console.log(countryCases)


	useEffect(() => {
		const map = new mapboxgl.Map({
		    //set the container for the map as the current component
		    container: mapContainerRef.current,
		    //set the style for the map
		    style: 'mapbox://styles/mapbox/streets-v11',
		    center: [44.63,28.77], //center ng map para makita 10 countries
		    zoom: 0
		    })

		    map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
		    
		    for(let counter=0; counter < 10; counter++){
		    	//data.country_name siya kasi nareretrieve natin yung buong data, so if we want the country name, we just need to specify na country_name :)	
				fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${countryNames[counter]}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
						.then(res => res.json())
						.then(data => {
							console.log(data)

							const marker = new mapboxgl.Marker()
						    .setLngLat([data.features[0].center[0],data.features[0].center[1]])
						    .addTo(map)
						})
		   }

		   return () => map.remove()
	}, [])

	return (
		<React.Fragment>
		<h1>10 Countries With The Highest Number Of Cases</h1>
		<Doughnut data={{
			datasets: [{
				data: [...countryCases],
				backgroundColor: ["red", "orange", "yellow", "green", "blue", "indigo", "violet", "black", "gray", "pink"]
			}],
			labels: [...countryNames]
		}} redraw={false}
		/>
		<div className="mapContainer" ref={mapContainerRef} />
		</React.Fragment>
	)
}
	//code ko
	/*console.log(data)

	const mapContainerRef = useRef(null)

	const countriesStats = data.countries_stat;
	console.log(countriesStats);

	const countriesCases = countriesStats.map(countryStat => {
		return {
			name: countryStat.country_name,
			cases: toNum(countryStat.cases)
		}
	})

	console.log(countriesCases)

	const countriesArray = countriesCases.sort((a,b) => {
		if(a.cases < b.cases){
			return 1 // a will move to the right
		} else if (a.cases > b.cases) {
			return -1 // a will move to the left
		} else {
			return 0
		}
	}).slice(0,10)

	console.log(countriesArray)

	useEffect(()=> {
		//Instantiate a new mapbox map object
		const map = new mapboxgl.Map({
			//set the container for the map as the current component
			container: mapContainerRef.current,
			//set the style for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [0,0],
			zoom: 1
		})

		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		countriesArray.forEach(country => {
			fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country.name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
				.then(res => res.json())
				.then(data => {

					const marker = new mapboxgl.Marker()
					.setLngLat([data.features[0].center[0],data.features[0].center[1]])
					.addTo(map)
				})
		})
	}, []);
	// reminder: ung }, [value]) nagrarun ung code everytime na may magbago dun sa loob ngarray, and blank array para di mag infinite loop!!!!

	return (
		<React.Fragment>
		<h1>10 Countries With The Highest Number Of Cases</h1>
		<Doughnut data={{
			datasets: [{
				data: countriesArray.map((country) => country.cases),
				backgroundColor: ["red", "orange", "yellow", "green", "blue", "indigo", "violet", "black", "gray", "pink"]
			}],
			labels: countriesArray.map((country) => country.name)
		}} redraw={false}
		/>
		<div className='mapContainer' ref={mapContainerRef} />
		</React.Fragment>
	)
}*/


export async function getStaticProps() {
  // fetch data from api endpoint
  const res = await fetch(
    'https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',
    {
      method: 'GET',
      headers: {
        'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
        'x-rapidapi-key': '6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539',
      },
    }
  );
  const data = await res.json();
  // return props
  return {
    props: {
      data
    },
  };
}