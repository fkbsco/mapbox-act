export default function toNum(str) {
	//convert the string to an array to get access to array methods
	// ... will spread or ihihiwalay siya (ex 2,456 will become ["2", "," , "4", "5", "6"])
	const arr = [...str]

	//will filter out the commas
	// ["2", "4", "5", "6"] 
	const filteredArr = arr.filter(element => element !== ",")

	// reduce the filtered array to a single string without the commas
	// "2456" to 2456
	return parseInt(filteredArr.reduce((x,y) => x + y))
}